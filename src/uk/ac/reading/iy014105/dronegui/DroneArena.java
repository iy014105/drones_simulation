package uk.ac.reading.iy014105.dronegui;

import java.util.ArrayList;
import java.util.Random;
import javafx.animation.AnimationTimer;
import javafx.scene.control.ListView;

public class DroneArena {
	//drone arena associated variables and definitions
	private int xmax, ymax;
	private Drone d;
	private DeathDrone e;
	private SonicDrone s;
	private Random r1 = new Random();
	public ArrayList<Drone> multidrone;
	public ArrayList<DeathDrone> DeathDrones;
	public ArrayList<SonicDrone> SonicDrones;
/**
 * constructor for the drone arena
 * @param width
 * @param height
 * contains the arraylists for the associated drones
 */
	public DroneArena(int width, int height) {
		xmax = width;
		ymax = height;
		multidrone = new ArrayList<Drone>(); // array constructor for drone
		DeathDrones = new ArrayList<DeathDrone>(); // array constructor for death drone
		SonicDrones = new ArrayList<SonicDrone>(); // array constructor for sonic drone
	}
	
	/**
	 * addDrone function
	 * will add a drone to array moving at a random direction
	 * @param mc
	 * @param Objects
	 */

	public void addDrone(MyCanvas mc, ListView<Drone> Objects) {
		//randomly generates direction and x and y based on arena
		Random RanGen;
		RanGen = new Random();
		int newx;
		int newy; // and
		do {
			newx = RanGen.nextInt(xmax - 40) - 1;// do randomly generates new X
													// & Y
			newy = RanGen.nextInt(ymax - 40) - 1;
		} while (getDroneAt(newx, newy) != null); // checks them against already
													// created drones using get
													// drone at if no drone at
													// location,
		d = new Drone(newx, newy, DirectionGUI.getRandom());
		multidrone.add(d);
		// new drone with unique X & Y and random direction
		mc.drawDrone(this);
		Animation.fillList(Objects);

	}
	/**
	 * addDeathDrone function
	 * will add a death drone to array moving at a random direction
	 * @param mc
	 * @param Objects
	 */

	public void addDeathDrone(MyCanvas mc, ListView<Drone> Objects) {
		Random RanGen;
		RanGen = new Random();
		int newx;
		int newy;
		do {
			newx = RanGen.nextInt(xmax - 40) - 1;// do randomly generates new X
													// & Y
			newy = RanGen.nextInt(ymax - 40) - 1;
		} while (getDroneAt(newx, newy) != null); // checks them against already
													// created drones using get
													// drone at if no drone at
													// location,
		e = new DeathDrone(newx, newy, DirectionGUI.getRandom());
		DeathDrones.add(e); // new drone with unique X & Y and random direction
		mc.drawDeathDrone(this);
		Animation.fillList(Objects);
	}
	
	/**
	 * addDeathDrone function
	 * will add a death drone to array moving at a random direction
	 * @param mc
	 * @param Objects
	 */

	public void addSonicDrone(MyCanvas mc, ListView<Drone> Objects) {
		Random RanGen;
		RanGen = new Random();
		int newx;
		int newy; // and
		do {
			newx = RanGen.nextInt(xmax - 40) - 1;// do randomly generates new X
													// & Y
			newy = RanGen.nextInt(ymax - 40) - 1;
		} while (getDroneAt(newx, newy) != null); // checks them against already
													// created drones using get
													// drone at if no drone at
													// location,
		s = new SonicDrone(newx, newy, DirectionGUI.getRandom());
		SonicDrones.add(s); // new drone with unique X & Y and random direction
		mc.drawSonicDrone(this);
		Animation.fillList(Objects);
	}
	//getters for x and y size for arena
	public int getXSize() {
		return xmax;
	}

	public int getYSize() {
		return ymax;
	}
	/**
	 * Death Function 
	 * removes drone from array once it has been 'killed'
	 * @param mc
	 */
	public void Death(MyCanvas mc) {
		//loops through drones and deathdrones
		for (DeathDrone e : DeathDrones) {
			for (Drone d : multidrone) {
				//checks to see if they are touching
				if (e.isTouching(d.getX(), d.getY())) {
					//remove drone from multidrone array
					multidrone.remove(d);
					//mc.ListView.remove(d);
				}
			}
		}
	}

	/**
	 * can move here checks if drone can move
	 * @param bx Integer:  this takes position of a drone in open area
	 * @param by
	 * @return
	 */
	public boolean canMoveHere(int bx, int by) {
		//checks to see drone location and anything in the way
		if (bx < 0 || bx >= xmax - 40 || by < 0 || by >= ymax - 40) {
			return false;
		}
		//loops through all drones 
		for (Drone d : multidrone) {
			for (DeathDrone e : DeathDrones)
				for (SonicDrone s : SonicDrones)
					if (getDroneAt(bx, by) != null) { // checks there is no
														// drone in
														// the
														// position it is
														// looking to
														// move into
						return false;
					}
			return true;
		}
		return (Boolean) null;
	}
	/**
	 * get drone at will check the x and y are unique
	 * @param x
	 * @param y
	 * @return
	 */
	public Drone getDroneAt(int x, int y) {
		for (Drone d : multidrone)
			for (DeathDrone e : DeathDrones)
				for (SonicDrone s : SonicDrones) // looks at drones in array
					//runs ishere to see drone locations
					if (d.isHere(x, y)) {
						if (e.isHere(x, y)) {
							if (s.isHere(x, y)) {
								return s;// calls isHere to check for unique X
											// and
							}
							return e;
						}
						// Y values
						return d; // returns drone if X & Y same
					}
		return null;
	}
	/**
	 * Move all drones will loop through the drones and try to move them
	 * @param mc
	 */
	public void moveAllDrones(MyCanvas mc) {
		//loops through all drones
		for (Drone d : multidrone) {
			for (DeathDrone e : DeathDrones) {
				for (SonicDrone s : SonicDrones) {
					//use try to move to move all the drones by 1
					d.tryToMove(this); // moves drone by 1 if it meets conditions
					e.tryToMove(this);
					s.tryToMoveSonic(this); //sonic drones move by 5 instead of 1
				}
				//updates canvas to give user up to date visual interpretation
				mc.updateCanvas(this);
				//calls death function
				Death(mc);
			}
		}
	}
}

