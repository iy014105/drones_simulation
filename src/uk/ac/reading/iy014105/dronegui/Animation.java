package uk.ac.reading.iy014105.dronegui;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Animation extends Application {
	private int CanvasX = 750, CanvasY = 500; 
	private MyCanvas mc;
	private static DroneArena myArena;
	private static AnimationTimer timer;

	/**
	 * Fill List function 
	 *  will add the drones to the list when drones are added
	 */
	public static void fillList(ListView<Drone> ListDrone){
		//clear the list view
		ListDrone.getItems().clear();
		//loop through drones and add them to the list view
			for (Drone d : myArena.multidrone)
				ListDrone.getItems().add(d);
			//loop through DeathDrones and adds them to the list view
			for (DeathDrone e : myArena.DeathDrones)
				ListDrone.getItems().add(e);
			//loops through sonic drones and add them to list view
			for (SonicDrone s : myArena.SonicDrones){
				ListDrone.getItems().add(s);
			}
		}
	;
 /**Start Function
  * holds all information surrounding buttons, list view and everything involved
  */
	public void start(Stage stagePrimary) throws Exception {
		stagePrimary.setTitle("Josh Buckle 27014105");

		Group root = new Group();
		//setting canvas details and adding it to the stage
		Canvas canvas = new Canvas(750, 500);
		root.getChildren().add(canvas);
		//creating the canvas using the x and y and adding the drone arena
		mc = new MyCanvas(canvas.getGraphicsContext2D(), CanvasX, CanvasY);
		myArena = new DroneArena(750, 500);
		mc.fillCanvas(CanvasX, CanvasY);
		
			//timer used to start and stop the simulation
		timer = new AnimationTimer() {
			public void handle(long now) {
				//will stop or start the moveAllDrones function
				myArena.moveAllDrones(mc);
			}
		};
		//List View Drone contains variables 'objects'
		ListView<Drone> Objects = new ListView<>();
		VBox vbList = new VBox();
		//sets positioning and formatting for the vblist
		vbList.getChildren().addAll(Objects);
		vbList.setAlignment(Pos.CENTER);
		vbList.setPadding(new Insets(0, 0, 0, 50));

		//button to add a normal drone
		Button AddDrone_btn = new Button("Add Drone");
		AddDrone_btn.setOnAction(e -> {
			//drone added to arena using my canvas functions and objects variable
			myArena.addDrone(mc, Objects);
		});
		//button formatting
		AddDrone_btn.setMinSize(125, 50);
		AddDrone_btn.setMaxSize(125, 50);

		//button for death drone
		Button AddDeathDrone_btn = new Button("Add Death Drone");
		AddDeathDrone_btn.setOnAction(e -> {
			//adds death drone to arena
			myArena.addDeathDrone(mc, Objects);
		});
		//button formatting
		AddDeathDrone_btn.setMinSize(125, 50);
		AddDeathDrone_btn.setMaxSize(125, 50);

		//button for adding a sonic drone
		Button AddSonicDrone_btn = new Button("Add Sonic Drone");
		AddSonicDrone_btn.setOnAction(e -> {
			//adds sonic drone to arena 
		myArena.addSonicDrone(mc, Objects);
		});
		//button formatting
		AddSonicDrone_btn.setMinSize(125, 50);
		AddSonicDrone_btn.setMaxSize(125, 50);

		//start button
		Button Start_btn = new Button("Start");
		//starts timer
		Start_btn.setOnAction(e -> timer.start());
		//formatting
		Start_btn.setMinSize(125, 50);
		Start_btn.setMaxSize(125, 50);

		//stop button
		Button Stop_btn = new Button("Stop");
		//stops timer
		Stop_btn.setOnAction(e -> timer.stop());
		//button formatting
		Stop_btn.setMinSize(125, 50);
		Stop_btn.setMaxSize(125, 50);

		//sets padding and formatting for console
		HBox hbButtons = new HBox(20);
		hbButtons.setAlignment(Pos.CENTER);
		hbButtons.setPadding(new Insets(0, 0, 50, 0));

		//adds all button to the console
		hbButtons.getChildren().addAll(AddDrone_btn, AddDeathDrone_btn, AddSonicDrone_btn, Start_btn, Stop_btn);

		//creates borderpane
		BorderPane borderPane = new BorderPane(); // New borderpane to store all
													// features

		//formats borderpane inserting items in certain areas of screen
		borderPane.setCenter(root);
		borderPane.setBottom(hbButtons);
		borderPane.setLeft(vbList);

		// Set the scene with the borderpane, with the size being the slightly
		// smaller than the screen.
		Scene scene1 = new Scene(borderPane, 1200, 700);

		stagePrimary.setScene(scene1); // Put the scene in the window

		stagePrimary.show();
	}

	public static void main(String[] args) {
		Application.launch(args); // launch the GUI
	}

}
