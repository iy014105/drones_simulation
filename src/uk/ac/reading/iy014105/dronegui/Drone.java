package uk.ac.reading.iy014105.dronegui;

import java.util.ArrayList;

import javafx.scene.image.Image;

/**
 * Drone class
 * 
 * @author iy014105 sets movement and info for the standard drone
 */
public class Drone {
	// drone associated variables
	protected int x;
	protected int y;
	protected int droneid;
	protected Image droneImage;
	private static int dronecount = 0;
	protected DirectionGUI direction = DirectionGUI.South;

	/**
	 * drone constructor
	 * 
	 * @param bx
	 * @param by
	 * @param dir
	 */
	public Drone(int bx, int by, DirectionGUI dir) { // constructor for drones
		x = bx;
		y = by;
		direction = dir;
		droneid = dronecount++;
		// each drone ID goes up by one each time new
		// drone added
	}

	/**
	 * getters and setters for the drones x and y
	 * 
	 * @return
	 */
	public int getX() {
		return x;
	}

	public int getY() { // getters for the x and y values
		return y;
	}

	/**
	 * gets randomised direction
	 * 
	 * @return
	 */
	public DirectionGUI getDirection() {
		return direction;
	}

	public boolean tryToMove(DroneArena myArena) {
		int bx = this.x;
		int by = this.y;
		// looks at direction drone is facing for movement
		if (this.direction == DirectionGUI.North) {
			by++;
		} else if (this.direction == DirectionGUI.East) {
			bx--;
		} else if (this.direction == DirectionGUI.South) {
			by--;
		} else if (this.direction == DirectionGUI.West) {
			bx++;
		} // each will move the
			// drone a certain
			// direction by 1
			// depending on
			// direction faced

		// checks to see if drone can move
		if (myArena.canMoveHere(bx, by)) {
			// moves drone to new position
			this.x = bx;
			this.y = by;
			return true;
		} else {
			// if drone cant be moved, turn the direction round one
			this.direction = DirectionGUI.getRandom();
			return false;
		}
	}

	public boolean isHere(int sx, int sy) {
		return sx == x && sy == y; // see if the new x and y are the same as any
									// previous drones
	}

	public boolean isTouching(int sx, int sy) {
		int radius = 20;
		if ((x - sx < radius && x - sx > -radius) && (y - sy < radius && y - sy > -radius)) { // Codnitions
																								// to
																								// delete
																								// the
																								// drone
			return true; // Deleting drone
		}
		return false;// see if the new x and y are the same as an // previous
						// drones
	}

	public String toString() {
		// gives info on drone in a string ready to be displayed in list view
		return "Drone " + droneid + " (" + x + ", " + y + ") going " + direction.toString(); // displays
																								// //
																								// dron
																								// //
																								// information
	}

	public void setXY(int nx, int ny) {
		x = nx;
		y = ny;
	}
}