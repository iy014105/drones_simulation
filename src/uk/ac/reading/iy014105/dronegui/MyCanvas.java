package uk.ac.reading.iy014105.dronegui;

import javafx.application.Application;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * MyCanvas will contain info based on drone images and canvas size
 * @author iy014105
 *
 */
public class MyCanvas extends Application {
	//variables and declarations related to the file
	int xCanvasSize = 1300; // constants for relevant sizes, default values set
	int yCanvasSize = 750;
	//Images loaded in at the start
	Image drone = new Image("Drone.png");
	Image DeathDrone = new Image("DeathDrone.png");
	Image SonicDrone = new Image("SonicDrone.png");
	GraphicsContext gc;

	/**
	 * constructor for canvas
	 */
	public MyCanvas(GraphicsContext g, int xcs, int ycs) {
		//canvas variables
		gc = g;
		xCanvasSize = xcs;
		yCanvasSize = ycs;
	}

	/**
	 * fill canvas 
	 * sets formatting for the canvas
	 * @param width
	 * @param height
	 */
	public void fillCanvas(int width, int height) {
		//sets colour, size and formatting for canvas
		gc.setFill(Color.LAVENDER);
		gc.fillRect(0, 0, width, height);
		gc.setStroke(Color.BLACK);
		gc.strokeRect(0, 0, width, height);
	}
	/**
	 * update canvas
	 * will update the canvas and re-draw drone images
	 * @param myArena
	 */
	public void updateCanvas(DroneArena myArena){
		//sets formatting and clears canvas
		gc.clearRect(0, 0, xCanvasSize, yCanvasSize);
		//fills the canvas
		fillCanvas(xCanvasSize, yCanvasSize);
		//draws all drones
		drawDrone(myArena);
		drawDeathDrone(myArena);
		drawSonicDrone(myArena);
		
	}
	
	/**
	 * draw drone
	 * draws images of drone
	 * @param myArena
	 */
	public void drawDrone(DroneArena myArena) {
		//loops through drone array multidrone
		for (Drone d : myArena.multidrone) {
			//draws images based on information inside of multidrone, giving size and location
			gc.drawImage(drone, d.getX(), d.getY(), 25, 25);
			}
		}
	/**
	 * draw death drone
	 * draws images of death drone
	 * @param myArena
	 */
	public void drawDeathDrone(DroneArena myArena){
		// loops through DeathDrones array 
		for (DeathDrone e : myArena.DeathDrones){
			//draws death drones based upon information in array, giving size and location
			gc.drawImage(DeathDrone,  e.getX(), e.getY(), 30, 30);
		}
	}
	
	/**
	 * draw sonic drone
	 * draws images of sonic drone
	 * @param myArena
	 */
	public void drawSonicDrone(DroneArena myArena){
		//loops through SonicDrones array
		for (SonicDrone s : myArena.SonicDrones){
			//draws sonic drones based on info in array, giving size and location
			gc.drawImage(SonicDrone,  s.getX(), s.getY(), 25, 25);
		}
	}

	public int getXCanvasSize() {
		return xCanvasSize;
	}

	public int getYCanvasSize() {
		return yCanvasSize;
	}


	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
