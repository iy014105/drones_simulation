package uk.ac.reading.iy014105.dronegui;

	import java.util.Random;
	/**
	 * direction class 
	 * @author iy014105
	 *
	 */

	public enum DirectionGUI {
		North, South, East, West;

		public static DirectionGUI getRandom() {
			Random r = new Random(); // function to get random directoin
			return values()[r.nextInt(values().length)];
		}
	}