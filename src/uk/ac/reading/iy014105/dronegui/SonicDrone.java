package uk.ac.reading.iy014105.dronegui;

import java.util.ArrayList;

public class SonicDrone extends Drone {
/**
 * SonicDrone 
 * extension of Drone class
 * @param bx
 * @param by
 * @param dir
 */
	public SonicDrone(int bx, int by, DirectionGUI dir) {
		super(bx, by, dir);
			
		}
	/**
	 * tryToMoveSonic
	 * same as tryToMove in drone, however speed is altered
	 * @param myArena
	 * @return
	 */
	public boolean tryToMoveSonic(DroneArena myArena) {
		int bx = this.x;
		int by = this.y;
		
		if (this.direction == DirectionGUI.North) {
			by +=3; // moves by 3 instead of 1 
		} else if (this.direction == DirectionGUI.East) {
			bx -=3;
		} else if (this.direction == DirectionGUI.South) {
			by -=3;
		} else if (this.direction == DirectionGUI.West) {
			bx +=3;
		} // each will move the
			// drone a certain
			// direction by 3
			// depending on
			// direction faced
		if (myArena.canMoveHere(bx, by)) {
			// moves drone to new position
			this.x = bx;
			this.y = by;
			return true;
		} else {
			// if drone cant be moved, turn the direction round one
			this.direction = DirectionGUI.getRandom();
			return false;
		}
	}
	/**
	 * tostring
	 * sonic drone specific string statement for use in list view
	 */
	public String toString() {
		return "Sonic Drone " + droneid + " (" + x + ", " + y + ") going " + direction.toString();
	}
}

