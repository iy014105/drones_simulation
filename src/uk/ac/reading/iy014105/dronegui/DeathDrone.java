package uk.ac.reading.iy014105.dronegui;

public class DeathDrone extends Drone{
/**
 * DeathDrone
 * extension of Drone class
 * @param bx
 * @param by
 * @param dir
 */
	public DeathDrone(int bx, int by, DirectionGUI dir) {
		super(bx, by, dir);
		// TODO Auto-generated constructor stub
	}
	/**
	 * toString
	 * creates string statement based around death drone information for use in list view
	 */
	public String toString() {
		return "Death Drone " + droneid + " (" + x + ", " + y + ") going " + direction.toString();
	}
		
}